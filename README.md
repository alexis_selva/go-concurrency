These are the programming assignments relative to Go - Concurrency

- Assignment 1: define Moore's law and describe the physical limitations in devices that have stopped it from continuing to be true
- Assignment 2: explore race conditions by creating and running two simultaneous goroutines
- Assignment 3: write a program to sort an array of integers
- Assignment 4: explore the design of concurrent algorithms and resulting synchronization issues

For more information, I invite you to have a look at https://www.coursera.org/learn/golang-concurrency

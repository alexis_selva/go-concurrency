package main

import (  
    "sync"
    "fmt"
)

type ChopS struct {
	sync.Mutex
}

type Philo struct {
	id		 	 int
	nbTries	     int
	leftCS  	*ChopS
	rightCS 	*ChopS
}

func getHostPermission() bool {
	ret := false

	// Lock the permission
	permission.Lock()
	if nbPermissions < 2 {
		nbPermissions++;
		ret = true
	}

	// Unlock the permission
	permission.Unlock()

	return ret
}

func (p Philo) eat() {

	// Evaluate the number of tries
	for p.nbTries < 3 {
	
		// Get permission to the host
		if getHostPermission() {
			
			// Lock
			p.leftCS.Lock()
			p.rightCS.Lock()
			fmt.Println("Starting to eat ", p.id)
		
			// Unlock
			fmt.Println("Finishing eating ", p.id)
			p.rightCS.Unlock()
			p.leftCS.Unlock()

			// Release the permission
			permission.Lock()
			nbPermissions--;
			permission.Unlock()

			// Increment the number of tries
			p.nbTries++
		}
	}

	wg.Done()
}

var (
	wg				sync.WaitGroup
	permission		sync.Mutex
	nbPermissions	int
)

func main() {
	Csticks := make([]*ChopS, 5)
	
	for i:= 0; i < 5; i++ {
		Csticks[i] = new(ChopS)
	}

	philos := make([]*Philo, 5)
	for i:= 0; i < 5; i++ {
		philos[i] = &Philo{i, 0, Csticks[i], Csticks[(i + 1) % 5]}
	}

	wg.Add(5)
	for i:= 0; i < 5; i++ {
		go philos[i].eat()
	}
	wg.Wait()
}

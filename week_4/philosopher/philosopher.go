package main

import (  
    "sync"
    "fmt"
)

type ChopS struct {
	sync.Mutex
}

type Philo struct {
	leftCS  *ChopS
	rightCS *ChopS
}

func (p Philo) eat() {
	for {
		p.leftCS.Lock()
		p.rightCS.Lock()

		fmt.Println("eating")

		p.rightCS.Unlock()
		p.leftCS.Unlock()
	}
}

var (
	wg sync.WaitGroup
)

func main() {
	Csticks := make([]*ChopS, 5)
	
	for i:= 0; i < 5; i++ {
		Csticks[i] = new(ChopS)
	}

	philos := make([]*Philo, 5)
	for i:= 0; i < 5; i++ {
		if i < (i + 1) % 5 {
			philos[i] = &Philo{Csticks[i], Csticks[(i + 1) % 5]}
		} else {	
			philos[i] = &Philo{Csticks[(i + 1) % 5], Csticks[i]}
		}
	}


	wg.Add(5)
	for i:= 0; i < 5; i++ {
		go philos[i].eat()
	}
	wg.Wait()
}

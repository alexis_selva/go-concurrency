package main

import (  
    "sync"
    "fmt"
)

var (
	wg sync.WaitGroup
    counter int32 
)

func print(keyword string, sli []int) {
	fmt.Print(keyword, " -> { ")
	len := len(sli) 
	if len > 0 {
		for i := 0; i < len - 1; i++ {
			fmt.Print(sli[i], ", ")
		}
		fmt.Print(sli[len - 1])
	}
	fmt.Println(" }")
}

func sort(sli []int) { 
	print("List to sort", sli)
	len := len(sli) 
    for i := 0; i < len; i++ {
		for j := i + 1; j < len; j++ {
			if sli[j] < sli[i] {
				tmp := sli[i]
				sli[i] = sli[j]
				sli[j] = tmp
			}
		}
	}
	
	wg.Done()
}

func merge(sli1 []int, sli2 []int) []int {  
	sli := append(sli1, sli2...)	
	print("List to merge", sli)
	len := len(sli) 
    for i := 0; i < len; i++ {
		for j := i + 1; j < len; j++ {
			if sli[j] < sli[i] {
				tmp := sli[i]
				sli[i] = sli[j]
				sli[j] = tmp
			}
		}
	}

	return sli	
}

func main() {
	var value int
	var len   int
	var index int

	// Create an empty slice of size 10
	slice := make([]int, 10)

	// Slice the slice
	slice = slice[:0]

	// For loop
	for {

		// Read an integer	
		fmt.Print("Enter an integer: ")
		_, err := fmt.Scanf("%d", &value)
		if err != nil {
			break
		}
		

		// Add the integer into the slice
		slice = append(slice, value)
		len++
	}

	// Sort the slice in 4 parts
	wg.Add(4)
	index = 0
	sli1 := slice[index:index + len / 4]
	go sort(sli1)
	index += len/4
	sli2 := slice[index:index + len / 4]
	go sort(sli2)
	index += len/4
	sli3 := slice[index:index + len / 4]
	go sort(sli3)
	index += len/4
	sli4 := slice[index:index + len / 4 + len % 4]
	go sort(sli4)
	wg.Wait()

	// Merge the slices
	sli12 := merge(sli1, sli2) 
	sli34 := merge(sli3, sli4)
	slice = merge(sli12, sli34) 

	// Print the entired sorted slice
	print("Entire sorted list", slice)

}

package main

import (  
    "sync"
    "fmt"
)

var (
	wg sync.WaitGroup
    counter int32 
)

func inc_100_1() {  
    for i := 0; i < 10000; i++ {
		value := counter
		value = value + 1
		counter = value
	}
	wg.Done()
}


func inc_100_2() {  
    for i := 0; i < 10000; i++ {
		value := counter
        value = value + 5
		counter = value
    }
	wg.Done()
}

func main() {
	wg.Add(2)
    go inc_100_1()
    go inc_100_2()
	wg.Wait()
    fmt.Println("Counter:", counter)
}
